extends Node

var score = 0 setget set_score

onready var scoreLabel = $ScoreLabel

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene("res://StartMenu.tscn")

func set_score(value):
	score = value
	update_score()

func _on_Enemy_score_up():
	self.score += 10

func update_score():
	scoreLabel.text = "Score: " + str(score)

func update_save_data():
	var save_data = SaveAndLoad.load_data_from_file()
	if score > save_data.highscore:
		save_data.highscore = score
		SaveAndLoad.save_data_to_file(save_data)
	

func _on_Ship_player_death():
	update_save_data()
	yield(get_tree().create_timer(1),"timeout")
	get_tree().change_scene("res://GameOverScreen.tscn")
