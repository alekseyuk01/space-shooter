extends Node

onready var highscoreLabel = $Highscore

func _ready():
	set_highscore_label()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene("res://StartMenu.tscn")

func set_highscore_label():
	var save_data = SaveAndLoad.load_data_from_file()
	highscoreLabel.text = "Highscore: " + str(save_data.highscore)


func _on_ReturnToMenu_pressed():
	get_tree().change_scene("res://StartMenu.tscn")
