extends Node

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		get_tree().change_scene("res://World.tscn")
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

func _on_StartButton_pressed():
	get_tree().change_scene("res://World.tscn")


func _on_HowToPlayButton_pressed():
	get_tree().change_scene("res://HowToPlayMenu.tscn")
